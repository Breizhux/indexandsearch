#! /usr/bin/env python3
#coding: utf-8

import os
import codecs
import index
import markdown
from fuzzywuzzy import process

#Index params
if os.name == "posix" :
    SEARCH_DIR = "{}/Images".format(os.environ['HOME'])
    MD_FILE = "{}/arborescence.md".format(os.getcwd())
    HTML_FILE = "{}/index.html".format(os.getcwd())
    MD_RESULT = "{}/search_result.md".format(os.getcwd())
    HTML_RESULT = "{}/search_result.html".format(os.getcwd())
else :
    SEARCH_DIR = "{}\\Downloads".format(os.environ['USERPROFILE'])
    MD_FILE = "{}\\arborescence.md".format(os.getcwd())
    HTML_FILE = "{}\\index.html".format(os.getcwd())
    MD_RESULT = "{}\\search_result.md".format(os.getcwd())
    HTML_RESULT = "{}\\search_result.html".format(os.getcwd())

def make_index() :
    index.arbo_to_markdown(SEARCH_DIR, MD_FILE)
    index.md_to_html(MD_FILE, HTML_FILE)

def search(term, result_nbr=5, min_match=75) :
    """ Search and return a list of best markdown
    match for the term"""
    names = {}
    with codecs.open(MD_FILE, 'r', 'utf-8') as file :
        for line in file.readlines() :
            if not "[dossier]" in line : continue
            name = line.replace("[dossier]", "").replace("\n", "")
            name = os.path.basename(name[name.index('](')+2:-1])
            if name in names :
                names[name].append(line.replace('\n', ''))
            else :
                names[name] = [line.replace('\n', '')]
    matchs = process.extract(term, names.keys(), limit=result_nbr)
    result = []
    for i in matchs :
        if i[1] >= min_match :
            lines = names[i[0]]
            for l in lines :
                mdline = "[{}%]{}".format(i[1], l)
                result.append(mdline)
    if len(result) == 0 :
        result.append("## Aucun résultat...".format(term))
    return result

def write_result(lines, term, remove_md=False) :
    """ Write the result in md file then html"""
    with codecs.open(MD_RESULT, 'w', 'utf-8') as file :
        file.write("## Résultats pour le terme : \"{}\"\n\n".format(term))
        for i in lines :
            file.write(i+"\n")
    markdown.markdownFromFile(input=MD_RESULT,
                              output=HTML_RESULT,
                              encoding='utf8')
    #Correction du code html
    with codecs.open(HTML_RESULT, 'r', 'utf-8') as file : html_code = file.read()
    with codecs.open(HTML_RESULT, 'w', 'utf-8') as file :
        file.write(html_code.replace("</a>\n", "</a>\n<br>\n"
                           ).replace("%]<a", "%] [<a"
                           ).replace("<br>\n<a", "<br>\n[<a"
                           ).replace("dossier</a>", "dossier</a>]"
                           )
        )
    if remove_md : os.remove(MD_RESULT)

if __name__ == "__main__" :
    make_index()
    term = "versaille orangerie"
    md_match = search(term)
    write_result(md_match, term)

