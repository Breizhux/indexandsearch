#! /usr/bin/env python3
#coding: utf-8

import os
import sys
import search
import tkinter as tk
from tkinter.filedialog import askdirectory
from time import sleep

def get_params() :
    window = tk.Tk()
    window.title("Chercher des documents")
    window.resizable(False, False)
    #VALUES
    close = False
    text = tk.StringVar()
    directory = tk.StringVar()
    index = tk.IntVar()
    nbr_result = tk.IntVar()
    sensitivity = tk.DoubleVar()
    #ZONES
    search_zone = tk.Label(window)
    folder_zone = tk.Label(window)
    number_zone = tk.Label(window)
    sensit_zone = tk.Label(window)
    button_zone = tk.Label(window)
    search_zone.grid(row=1, column=4, padx=10, pady=10)
    folder_zone.grid(row=2, column=4, padx=10, pady=10)
    number_zone.grid(row=3, column=4, padx=10, pady=10)
    sensit_zone.grid(row=4, column=4, padx=10, pady=10)
    button_zone.grid(row=5, column=4, padx=10, pady=10)
    #TERM
    tk.Label(search_zone, text="Termes :").grid(row=4, column=1)
    text_box = tk.Entry(search_zone, textvariable=text, width=46)
    text_box.focus()
    text_box.grid(row=4, column=2)
    tk.Button(search_zone, text="Clean", command=lambda: text.set('')).grid(row=4, column=3)
    #FOLDER
    tk.Label(folder_zone, text="Dossier :").grid(row=4, column=1)
    folder_box = tk.Entry(folder_zone, textvariable=directory, width=40)
    folder_box.grid(row=4, column=2)
    check_index = tk.Checkbutton(folder_zone, text="Refaire l'index", variable=index)
    index.set(1)
    check_index.grid(row=5, column=2)
    tk.Button(folder_zone, text="Sélectionner...", command=lambda: directory.set(askdirectory())
        ).grid(row=4, column=3)
    directory.set(os.getcwd())
    #RESULT NUMBER
    tk.Label(number_zone, text="Nombre de résultat :").grid(row=4, column=1)
    spin_box = tk.Spinbox(number_zone, from_=0, to=1000, textvariable=nbr_result)
    nbr_result.set(5)
    spin_box.grid(row=4, column=2)
    tk.Label(number_zone, text=" "*46).grid(row=4, column=3)
    #sensitILITY
    tk.Label(sensit_zone, text="Sensibilité :").grid(row=4, column=1)
    sensit = tk.Scale(sensit_zone, variable=sensitivity, orient=tk.HORIZONTAL, length=300)
    sensitivity.set(75)
    sensit.grid(row=4, column=2)
    tk.Label(sensit_zone, text=" "*27).grid(row=4, column=3)
    #BUTTONS
    tk.Button(button_zone, text="Fermer", command=lambda: text.set('~&close&~')
        ).grid(row=4, column=1, padx=10)
    tk.Button(button_zone, text="Chercher", command=window.destroy
        ).grid(row=4, column=2, padx=10)

    while True :
        try :
            window.update()
            term = text_box.get()
            folder = directory.get()
            do_index = index.get()
            number = spin_box.get()
            sensit = sensitivity.get()
        except : return term, folder, int(do_index), int(number), int(sensit)
        sleep(0.05)
        if term == "~&close&~" :
            window.destroy()
            return None

def make_search(text, folder, index, number_result, sensitivity, ) :
    search.SEARCH_DIR = folder
    if index or not os.path.exists(search.MD_FILE) :
        print("[index] Create the index of files...")
        search.make_index()
    if text == "" :
        print("[search] No search. Just create the index of files.")
        return search.HTML_FILE
    print("[search] \"{}\", {}, {}".format(text, number_result, sensitivity))
    md_matchs = search.search(text, result_nbr=number_result, min_match=sensitivity)
    [print("    {}".format(i)) for i in md_matchs]
    search.write_result(md_matchs, text)
    os.remove(search.MD_RESULT)
    return search.HTML_RESULT

def open_browser(file) :
    print("[open] {}".format(file))
    if os.name == "posix" :
        if os.path.exists("/usr/bin/firefox-esr") :
            command = "firefox-esr \"{}\"".format(file)
        else :
            command = "firefox \"{}\"".format(file)
    else :
        command = "start firefox \"{}\"".format(file)
    os.system(command)

if __name__ == "__main__" :
    params = get_params()
    if params is None : sys.exit(1)
    file = make_search(*params)
    open_browser(file)
    sys.exit(0)
