#! /usr/bin/env python3
#coding: utf-8

import os
import codecs
import pathlib
import markdown

LEVEL_NUMBER = 5
PATH_SEPARATOR = "/" if os.name == "posix" else "\\"

def write_md(output, line, level=None) :
    """ Write line in markdown file.
    If level is None : is a simple line."""
    if level is not None :
        if level > LEVEL_NUMBER :
            line = "\n{} **{}**\n".format('  '*level, line)
        else :
            line = "\n\n{} {}\n".format('#'*level, line)
    elif "(\\\\" in line :
        line = line.replace("\\", "/")
        line = line.replace("(//", "(file://///")
    with codecs.open(output, 'a', 'utf-8') as file :
        file.write(line)

def get_files(folder_path=".") :
    ls_r = []
    for path, dirs, files in os.walk(folder_path):
        for filename in files:
            ls_r.append("{}{}{}".format(path, PATH_SEPARATOR, filename))
    return sorted(ls_r)

def arbo_to_markdown(path, output) :
    """ Create a markdown file to list file and dirs """
    os.chdir(path)
    with codecs.open(output, 'w', 'utf-8') as f :
        f.write("# Arborescence de "+os.path.basename(path))
    writed_dirs = []
    latest_dir = ""
    for file in get_files() :
        if os.path.dirname(file) != latest_dir :
            latest_dir = os.path.dirname(file)
            write_md(output, '\n\n')
        #Ajoute le titre du dossier si n?cessaire
        split_path = file.split(PATH_SEPARATOR)[1:-1]
        for i, j in enumerate(split_path) :
            work_path = "/".join(split_path[:i+1])
            if not work_path in writed_dirs :
                write_md(output, j, level=i+2)
                writed_dirs.append(work_path)
        #Ajoute le titre du fichier avec le lien
        abs_path = "{}{}".format(path, file[1:])
        line = "[dossier]({}) [{}]({})\n".format(
            os.path.dirname(abs_path), os.path.basename(abs_path), abs_path)
        write_md(output, line)

def md_to_html(input, output) :
    """ Convert a markdown file to html """
    #Convertion de md ? html
    markdown.markdownFromFile(input=input,
                              output=output,
                              encoding='utf8')
    #Correction du code html
    with codecs.open(output, 'r', 'utf-8') as file :
        html_code = file.read()
    with codecs.open(output, 'w', 'utf-8') as file :
        file.write(html_code.replace("</a>\n", "</a>\n<br>\n"
                           ).replace("<p><a", "<p>[<a"
                           ).replace("<br>\n<a", "<br>\n[<a"
                           ).replace("dossier</a>", "dossier</a>]"
                           )
        )

if __name__ == "__main__" :
    print("--For test !--")
    initial_dir = "/home/user/Images"
    md_output = "arborescence.md"
    html_output = "output.html"

    arbo_to_markdown(initial_dir, md_output)
    md_to_html(md_output, html_output)


