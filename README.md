# IndexAndSearch

Minimalist tool for searching files in a folder tree.

The search is based on file names only. The terms used are searched with an approximate match.
The index is created on the fly if it doesn't exist, or if the user checks the corresponding option.


## Getting started

Install :
```bash
git clone https://gitlab.com/Breizhux/indexandsearch
cd indexandsearch
virtualenv ./
source bin/activate
pip install -r requirements.txt
```

Run :
```bash
python3 gui.py
```
